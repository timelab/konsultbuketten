<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * Methods for TimberHelper can be found in the /functions sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
$context = array(
  'post' => array(
    'post_title' => '404 - Sidan kunde inte hittas'  
  ),
);

Timber::render( '404.twig', $context );

<?php
/**
 * The Template for displaying all single posts
 *
 *
 * @package  WordPress
 * @subpackage  Timber
 */

$permalink = get_permalink();
$data['facebook_share'] = "https://www.facebook.com/sharer/sharer.php?u=" . $permalink;
$data['sidebar_posts'] = get_posts_by_type('post', array('post_per_page' => 5, 'order' => 'DESC'));

if(array_key_exists('post_type', $data) && $data['post_type'] == 'page'){
  Timber::render( array( 'sidebar-subpage.twig' ), $data );
}else{
  Timber::render( array( 'sidebar-news.twig' ), $data );
}
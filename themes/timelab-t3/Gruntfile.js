'use strict';
module.exports = function(grunt)
{
  require('load-grunt-tasks')(grunt);

  grunt.initConfig(
  {
      theme_path: '.',
      sass: {
        main: {
          options: {
            includePaths: ['node_modules'],
            cleancss: true,
            compress: true
          },
          files: {
            '<%= theme_path %>/assets/css/main.min.css': '<%= theme_path %>/assets/scss/app.scss',
            '<%= theme_path %>/assets/css/admin.min.css': '<%= theme_path %>/assets/scss/admin.scss'
          }
        }
      },
      browserify: {
        dist: {
          files: {
            // destination for transpiled js : source js
            "<%= theme_path %>/assets/js/app.min.js": "<%= theme_path %>/assets/js/app.js",
          },
          options: {
            transform: [['babelify', { presets: "es2015" }]],
            browserifyOptions: {
              debug: true
            }
          }
        }
      },
      watch: {
        scss: {
          files: [
            '<%= theme_path %>/assets/scss/*.scss',
            '<%= theme_path %>/assets/scss/**/*.scss',
          ],
          tasks: ['sass:main', 'postcss:main']
        },
        js: {
          files: [
          'Gruntfile.js',
          '<%= theme_path %>/assets/js/*.js',
          '<%= theme_path %>/admin/assets/js/*.js',
          '<%= theme_path %>/assets/js/vendor/*.js',
          '<%= theme_path %>/assets/js/templates/*.js',
          '!<%= theme_path %>/assets/js/app.min.js',
          ],
          tasks: ['browserify']
        }
    },
      clean: {
        dist: [
          '<%= theme_path %>/assets/css/main.min.css',
          '<%= theme_path %>/assets/js/app.min.js'
        ]
    },
    browserSync: {
        bsFiles: {
            src : [
              '<%= theme_path %>/assets/css/*.css',
              '<%= theme_path %>/assets/css/*min.js',
            ]
        },
        options: {
            proxy:'<%= bebop.stages.development.site_url %>',
            watchTask: true,
            baseDir: "./"
        }
    },
    postcss: {
      main: {
        options: {
          map: {
              inline: false, // save all sourcemaps as separate files...
              annotation: '<%= theme_path %>/assets/css/' // ...to the specified directory
          },

          processors: [
            require('pixrem')(), // add fallbacks for rem units
            require('autoprefixer')(), // add vendor prefixes
            require('cssnano')() // minify the result
          ]
        },

        src: '<%= theme_path %>/assets/css/*.css'
      }
    },
  });

  // Load tasks
  //grunt.loadTasks('tasks');
  grunt.loadNpmTasks('grunt-sass')
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-postcss');

  // Register tasks
  grunt.registerTask('default',
  [
    'sass:main',
    'postcss:main',
    'browserify'
  ]);
  grunt.registerTask('dev',
  [
    'browserSync',
    'watch'
  ]);
};
$(function(){
	let mapOptions;

	if ( $(window).width() < 1024) {
	  mapOptions = {
	    zoom: 14,
	    streetViewControl: false,
      disableDefaultUI:true,
      draggable: false, zoomControl: false, scrollwheel: false, disableDoubleClickZoom: true,
	    center: new google.maps.LatLng(mapLat,mapLong),
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	}
	else {
 	  mapOptions = {
	    scrollwheel: false,
	    streetViewControl: false,
      disableDefaultUI:true,
	    zoom: 14,
	    zoomControl: true,
	    center: new google.maps.LatLng(mapLat,mapLong),
	    mapTypeId: google.maps.MapTypeId.ROADMAP
	  };
	}

	initialize(mapOptions);
	
	
	//Set Tabclicks
	$('.mapTabs li a').each(function(){
		$(this).click(function(){
			beachMarker.setPosition(new google.maps.LatLng( $(this).attr('data-lat'), $(this).attr('data-long')));
			responsiveMap.panTo(new google.maps.LatLng( $(this).attr('data-lat'), $(this).attr('data-long')));
		});
	});
	
	
});



function initialize(mapOptions) {

	 var styles = [
    {
      "featureType": "administrative",
      "elementType": "labels",
      "stylers": [
        {
          "color": "#6d6d6d"
        }
      ]
    },
    {
      "featureType": "administrative",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#ffffff"
        }
      ]
    },
    {
      "featureType": "landscape.man_made",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "landscape.natural",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "on"
        },
        {
          "color": "#f2f2f2"
        }
      ]
    },
    {
      "featureType": "poi",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#cfcdcc"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.text",
      "stylers": [
        {
          "color": "#f7640f"
        }
      ]
    },
    {
      "featureType": "road",
      "elementType": "labels.text.stroke",
      "stylers": [
        {
          "color": "#ffffff"
        }
      ]
    },
    {
      "featureType": "transit.station",
      "elementType": "all",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "geometry.fill",
      "stylers": [
        {
          "color": "#c4e0ec"
        }
      ]
    },
    {
      "featureType": "water",
      "elementType": "labels",
      "stylers": [
        {
          "visibility": "off"
        }
      ]
    }
	];

	var styledMap = new google.maps.StyledMapType(styles,{name: "Styled Map"});

	responsiveMap = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

	responsiveMap.mapTypes.set('map_style', styledMap);
  responsiveMap.setMapTypeId('map_style');

	//var image = baseUrl + '/assets/img/map-marker.png';
  var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
 	var myLatLng = new google.maps.LatLng(mapLat,mapLong);
 	beachMarker = new CustomMarker({
    position: myLatLng,
    map: responsiveMap,
  });
 	 
 	 
 	 //Start centercheck
 	setMapcenter();
 	 
}

var mapTimer;
function setMapcenter(){
	$(window).resize(function() {
		window.clearTimeout(mapTimer);
		mapTimer = window.setTimeout(function(){
			responsiveMap.panTo(beachMarker.position);
		},200);	
	});
}

CustomMarker.prototype = new google.maps.OverlayView();

function CustomMarker(opts) {
    this.setValues(opts);
}

CustomMarker.prototype.draw = function() {
    var self = this;
    var div = this.div;
    if (!div) {
        div = this.div = $('' +
            '<div class="custom-marker">' +
            '<div class="shadow"></div>' +
            '<div class="ball">' +
            '</div>' +
            '</div>' +
            '')[0];
        this.pinWrap = this.div.getElementsByClassName('pin-wrap');
        this.pin = this.div.getElementsByClassName('pin');
        this.pinShadow = this.div.getElementsByClassName('shadow');
        div.style.position = 'absolute';
        div.style.cursor = 'pointer';
        var panes = this.getPanes();
        panes.overlayImage.appendChild(div);
        google.maps.event.addDomListener(div, "click", function(event) {
            google.maps.event.trigger(self, "click", event);
        });
    }
    var point = this.getProjection().fromLatLngToDivPixel(this.position);
    if (point) {
        div.style.left = point.x + 'px';
        div.style.top = point.y + 'px';
    }
};



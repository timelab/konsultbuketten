$(document).ready(() => {
  $('.nav-toggle').on('click', function(e){
    e.preventDefault();
    open_menu();
  });

  $('.site-nav__close').on('click', function(e){
    e.preventDefault();
    close_menu();
  })

  $('.site-nav__link').on('click', function(e){
    if($(this).data('toggle') == "collapse"){
      $(this).parent().toggleClass('site-nav__item--active');
      $(this).next().collapse("toggle");
    }
  });
});

function close_menu(){
  $('.site-nav').css({
    'transform': ''
  });


  $('body').css('margin-left', '');
  $('html').css('overflow', '');

  $('.site-nav__overlay').addClass('site-nav__overlay--fadeOut').on('webkitTransitionEnd mozTransitionEnd MSTransitionEnd otransitionend transitionend', function(){
    $(this).removeClass('site-nav__overlay--open');
    $(this).removeClass('site-nav__overlay--fadeOut');
    $(this).off();
  });
};

function open_menu(){
  $('html').css({
    'overflow': 'hidden'
  });

  let nav_with = $('.site-nav').width();
  let nav_post = $('body').width() - nav_with;
  let container_right_pos = $('header.header').outerWidth() + parseFloat($('header.header').css('margin-left'));
  let nav_overflow = container_right_pos -nav_post;

  $('.site-nav').css({
    'transform': 'translate3d(0, 0, 0)',
  });

  if ($(window).width() > 565){
    if(nav_overflow > 0){
      $('body').css({
        'margin-left': '-' + (nav_overflow * 2) + 'px',
        'overflow': 'hidden'
      })

      
    }
  }

  $('.site-nav__overlay').addClass('site-nav__overlay--fadeIn').on('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
    $(this).addClass('site-nav__overlay--open');
    $(this).removeClass('site-nav__overlay--fadeIn');
    $(this).off();

    $('.site-nav__overlay').on('click', function(){
      close_menu();
    });
  });
};
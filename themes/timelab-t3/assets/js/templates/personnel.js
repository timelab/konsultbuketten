$(document).ready(() => {
  if($('.personnel__item').length > 0){
    $(document).on('click', function(e) {
      $('.personnel__item').each(function(){
        if($(e.target).parents('.personnel__item').length > 0){
          var target = $(e.target).parents('.personnel__item');
        }else if ($(e.target).hasClass('personnel__item')){
          var target = $(e.target);
        }else{
          var target = null
        }

        if(target != null){
          if($(this)[0] != target[0]){
            $(this).removeClass('personnel__item--open');
          }
        }else{
          $(this).removeClass('personnel__item--open');
        }
      });
    });

    $('.personnel__item').on('click', function(){
      $(this).addClass('personnel__item--open');
    });
  }
});
$(document).ready(() => {
  if($('.repeatable-posts__post').length > 0){
    $('.repeatable-posts__post').each(function(){
      if($('.repeatable-posts__title', this).height() > 30){
        if(!$('.repeatable-posts__text-box', this).hasClass('repeatable-posts__text-box--overlay')){
          $('.repeatable-posts__text', this).addClass('repeatable-posts__text--hidden-sm');
        }
      }
    });
  }
});
$(document).ready(() => {
  $('.nav-toggle').on('mouseenter', function(){
    $('g rect animate.mouseenter', this).each(function(){
      $(this)[0].beginElement();
    });
  })

  $('.nav-toggle').on('mouseleave', function(){
    $('g rect animate.mouseleave', this).each(function(){
      $(this)[0].beginElement();
    });
  })
});
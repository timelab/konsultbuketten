$(document).ready(() => {
  $(".sidebar-module__input").on('focus', function(){
    $(this).siblings('.sidebar-module__icon').addClass("sidebar-module__icon--hover");
  });

  $(".sidebar-module__input").on('focusout', function(){
    $(this).siblings('.sidebar-module__icon').removeClass("sidebar-module__icon--hover");
  });
});
window.jQuery = window.$ = require('jquery');
require('popper.js');
require('bootstrap');
require('./vendor/slick.js');

$(document).ready(() => {
	$('.slider').slick({
		'arrows': false,
		'dots': true,
    'dotsClass': 'slick-dots slider__dots' ,
    "autoplay": true,
    "autoplaySpeed": 7000,
		customPaging: function(slider, i) {
      return ''; // Remove button, customize content of "li"
    }
	});

  if($('header.header').length > 0){
    require('./templates/header.js');
  }

  if($('footer.footer').length > 0){
    require('./templates/footer.js');
  }

  if($('.site-nav').length > 0){
    require('./templates/menu.js');
  }
	
  if($('body.page-template-contactpage').length > 0){
    require('./mapfunctions.js');
	}

  if($('.repeatable-posts').length > 0){
    require('./templates/repeatable_posts.js');
  }

  if($('.personnel').length > 0){
    require('./templates/personnel.js');
  }
	
	// JS for news listing view
	if($('body.page-template-nyheter, body.single-post').length > 0){
    require('./news.js');
  }
});
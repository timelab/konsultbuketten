<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php') ) . '</a></p></div>';
	});
	
	add_filter('template_include', function($template) {
		return get_stylesheet_directory() . '/static/no-timber.html';
	});
	
	return;
}

require_once 'lib/post-types.php';
require_once 'lib/custom-functions.php';
require_once 'lib/routes.php';

Timber::$dirname = array('templates', 'views');

class StarterSite extends TimberSite {

	function __construct() {
		add_theme_support( 'post-formats' );
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'init', array( $this, 'register_option_pages' ) );
		add_action( 'init', array( $this, 'add_custom_thumbnail_sizes' ) );
		add_action( 'after_setup_theme', array( $this, 'load_textdomain' ) );
		add_action( 'after_setup_theme', array($this, 'register_my_menu') );
		add_action( 'admin_enqueue_scripts', array($this, 'admin_enqueue_scripts') );
		add_action( 'wp_enqueue_scripts', array($this, 'enqueue_scripts') );
		parent::__construct();
	}

	function add_custom_thumbnail_sizes(){
		add_image_size( 'startpage_slide', 1280, 619, true);
		add_image_size( 'startpage_slide_sm', 768, 399, true);
		add_image_size( 'startpage_slide_xs', 575, 292, true);
		add_image_size( 'startpage_half_page', 640, 383, true);
		add_image_size( 'startpage_half_page_xs', 575, 280.37, true);
		add_image_size( 'repeatable_post_portrait', 426, 529, true);
		add_image_size( 'repeatable_post_landscape', 427, 393, true);
		add_image_size( 'repeatable_post_xs', 575, 220.8, true);
		add_image_size( 'personnel', 279, 356, true);
		add_image_size( 'personnel_md', 337, 459.75, true);
		add_image_size( 'personnel_sm', 501, 683.74, true);
		add_image_size( 'subpage_full', 1280, 519, true);
	}

	function register_post_types() {
		/**
		 * this is where you can register custom post types
		 * - - - - - - - - - - - - - - - - -
		 * Format for making a custom post type
		 * makeCustomPostType('singular', 'plural', 'icon', has single)
		 * -------------------------------------------------
		 * singluar = lowecase post type name in singular
		 * plural = same as singular but in plural form
		 * icon = code for icon can be src to an image or dashicon code
		 * Dash icons = https://developer.wordpress.org/resource/dashicons/
		 * has single = bool if the post type has single pages
	   */
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function register_option_pages(){
		if( function_exists('acf_add_options_page') ) {
			acf_add_options_page(array(
				'page_title' 	=> __('Theme Settings', 'timelab-t3'),
				'menu_title'	=> __('Theme Settings', 'timelab-t3'),
				'menu_slug' 	=> 'theme-general-settings',
				'capability'	=> 'edit_posts',
				'redirect'		=> false
			));
				
			// acf_add_options_sub_page(array(
			// 	'page_title' 	=> __('Footer', 'timelab-t3'),
			// 	'menu_title'	=> __('Footer', 'timelab-t3'),
			// 	'parent_slug'	=> 'theme-general-settings',
			// ));
		}
	}

	function register_my_menu() {
	  register_nav_menu( 'primary', __( 'Primary Menu', 'theme-slug' ) );
	}

	function load_textdomain() {
		load_theme_textdomain( 'timelab-t3', get_theme_file_path('/lang') );
	}

	function add_to_context( $context ) {
		$context['menu'] = new TimberMenu('primary');
		$context['site'] = $this;
		$context['menu_item'] = $this->get_current_menu_item($context['menu']->items);
		$context['current_theme_template'] = pathinfo(get_page_template())['filename'];
		$context['google_api_key'] = get_field( 'acf_option_google_api_key', 'options' );
		$context['footer_message'] = get_field( 'acf_footer_message', 'options' );
		return $context;
	}

	function get_current_menu_item($menu_items){
		$child = false;
		$object = false;

		foreach ($menu_items as $item) {
			if($object == false){
				if($item->children != null){
					if($child == false){
						$child = $this->get_current_menu_item($item->children);
					}
				}

				if ($child) {
					$object = $item;
				}else{
					if($item->current){
						$object = $item;
					}else{
						$object = false;
					}
				}
			}
		}

		return $object;
	}

	function prepend_theme_path($string) {
		return get_theme_file_path($string);
	}

	function prepend_theme_uri($string) {
		return get_theme_file_uri($string);
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own functions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('theme_path', new Twig_SimpleFilter('theme_path', array($this, 'prepend_theme_path')));
		$twig->addFilter('theme_uri', new Twig_SimpleFilter('theme_uri', array($this, 'prepend_theme_uri')));
		return $twig;
	}

	function admin_enqueue_scripts() {
    wp_enqueue_style('my-admin-style', get_theme_file_uri('/assets/css/admin.min.css'));
	}

	function enqueue_scripts() {
		wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,700,900', false ); 
		wp_enqueue_style( 'site-style', get_theme_file_uri('/assets/css/main.min.css'));
		wp_enqueue_script( 'site-scripts', get_theme_file_uri('/assets/js/app.min.js'));
	}
}

new StarterSite();

/* Custom Setting page */
function add_admin_menu_separator($position) {
    global $menu;
    $index = 0;
    if(is_array($menu)){
	    foreach($menu as $offset => $section) {
	        if (substr($section[2],0,9)=='separator')
	        $index++;
	        if ($offset>=$position) {
	            $menu[$position] = array('','read',"separator{$index}",'','wp-menu-separator');
	            break;
	        }
	    }
	    ksort( $menu );
  	}
}

add_action('admin_init','admin_menu_separator');
function admin_menu_separator() {add_admin_menu_separator('80.015');}


function my_acf_init() {
	acf_update_setting('google_api_key', get_field( 'acf_option_google_api_key', 'options' ));
}
add_action('acf/init', 'my_acf_init');

// get the the role object
$editor = get_role('editor');
// add $cap capability to this role object
$editor->add_cap('edit_theme_options');

// Add ACF script to admin footer
function my_acf_input_admin_footer() {

?>
<script type="text/javascript">
(function($) {

    $( ".values" ).sortable({
      connectWith: ".values",
    })

    function GetSubstringIndex(str, substring, n) {
	      var times = 0, index = null;

	      while (times < n && index !== -1) {
	          index = str.indexOf(substring, index+1);
	          times++;
	      }

	      return index;
	  }
	  
	  acf.add_action('sortstop', function ($el) {
      if ($($el).parents('[data-name="columns"] > .acf-input > .acf-repeater').length) {
      	console.log($($el).parents('.acf-row'));
        var row_num = $($($el).parents('.acf-row').last()).attr('data-id');
        var column_num = $($el).closest('.acf-row').attr('data-id');
        $($el).find('[name^="acf[field_"]').each(function(){
          var field_name = $(this).attr('name');
          var field_nameArray = field_name.replace(/]/g, "").split("[");
          var new_name = field_nameArray[0] + "[" + field_nameArray[1] + "][" + row_num + "][" + field_nameArray[3] + "][" + column_num + "][" + field_nameArray[5] + "][" + field_nameArray[6] + "][" + field_nameArray[7] + "]";
          $(this).attr('name', new_name);
        });
        $($el).closest('[data-name="modules"]').find('.acf-input > .acf-flexible-content > .values > .layout').each(function(index){
          $(this).find('[name^="acf[field_"]').each(function(){
            var field_name = $(this).attr('name');   
            var index_location = GetSubstringIndex(field_name,']', 5)+2;
            var new_name = field_name.substr(0, index_location) + index + field_name.substr(index_location+1);
            $(this).attr('name', new_name);
          });
        });
      }
    });          


})(jQuery); 
</script>
<?php

}

add_action('acf/input/admin_footer', 'my_acf_input_admin_footer');
<?php
  function get_posts_by_type($post_type, $options = array()){
    $args = array(
      'post_type'      => $post_type,
      'order'          => 'ASC',
    );

    $args = array_merge($args, $options);

    $posts_array = Timber::get_posts( $args );
    foreach ($posts_array as $key => $single) {
      $posts_array[$key]->acf = get_fields($single->ID);
    }

    return $posts_array;
  }

?>
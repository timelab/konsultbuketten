<?php /* Template Name: Kontaktsida */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['acf'] = get_fields();

$context['acf']['acf_contact_address']['full_address'] = $context['acf']['acf_contact_address']['address'];
$address = explode(", ", $context['acf']['acf_contact_address']['address']);
$context['acf']['acf_contact_address']['address'] = $address[0];
$context['acf']['acf_contact_address']['city'] = $address[1];
$context['acf']['acf_contact_address']['country'] = $address[2];

Timber::render( 'contactpage.twig', $context );
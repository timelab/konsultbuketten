<?php /* Template Name: Startsida */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['acf'] = get_fields();
Timber::render( 'startpage.twig', $context );
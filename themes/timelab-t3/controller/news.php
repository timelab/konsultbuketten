<?php /* Template Name: Nyheter */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['acf'] = get_fields();
$context['posts'] = [];
$context['sidebar'] = Timber::get_sidebar('sidebar.php');
$context['pagination'] = ""; 

$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$args = array(
  'post_type'      => 'post',
  'order'          => 'DESC',
  'posts_per_page' => 3,
  'paged'          => $paged
);

$the_query = new WP_Query( $args );

if ( $the_query->have_posts() ) {
  while ( $the_query->have_posts() ) {
    $the_query->the_post();
    $post_id = get_the_id();
    $context['posts'][$post_id] = Timber::get_post($post_id);
    $context['posts'][$post_id]->acf = get_fields($post_id);
  }

  $total_pages = $the_query->max_num_pages;
   
  if ($total_pages > 1){
    $current_page = max(1, $paged);

    $context['pagination'] = paginate_links(array(
      'base' => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
      'format' => '?paged=%#%',
      'current' => $current_page,
      'total' => $total_pages,
      'type'  => 'array',
      'prev_next' => false,
    ));
  }

  wp_reset_postdata();
}

Timber::render( 'news.twig', $context );